# Run StarryNightSky Deployment using Docker-Compose

How To Run:
1. Run `docker-compose up -d`

How To Destroy:
1. Run `docker-compose down -v`